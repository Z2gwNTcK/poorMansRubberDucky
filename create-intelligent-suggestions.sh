#!/bin/bash
# Bash script which is used to create completely randomized VCF files for intelligent suggestions used natvely in OSX Mail.
# Once the VCF files are created, they must be moved to ~/Library/Metadata/com.apple.IntelligentSuggestions/ in order for them to
# be used.

LAST_NAMES=("smith" "johnson" "williams" "jones" "brown" "davis" "miller" "wilson" "moore" "taylor" "anderson" "thomas" "jackson" "white" "harris" "garcia" "rodriguez" "martinez" "hernandez" "lopez" "gonzalez" "file" "torbin" "morales");
FIRST_NAMES=("james" "mary" "john" "patricia" "robert" "jennifer" "michael" "linda" "william" "elizabeth" "david" "barbara" "richard" "susan" "joseph" "jessica" "thomas" "sarah" "charles" "margaret" "christopher" "karen" "daniel" "nancy" "matthew" "lisa" "anthony" "betty" "donald" "dorothy" "mark" "sandra" "paul" "ashley" "steven" "kimberly" "andrew" "donna" "kenneth" "emily" "george" "carol" "joshua" "michelle" "kevin" "amanda" "brian" "melissa" "edward" "deborah" "ronald" "stephanie" "timothy" "rebecca" "jason" "laura" "jeffrey" "helen" "ryan" "sharon" "jacob" "cynthia" "gary" "kathleen" "nicholas" "amy" "eric" "shirley" "stephen" "angela" "jonathan" "anna" "larry" "ruth" "justin" "brenda" "scott" "pamela" "brandon" "nicole" "frank" "katherine" "benjamin" "samantha" "gregory" "christine" "raymond" "catherine" "samuel" "virginia" "patrick" "debra" "alexander" "rachel" "jack" "janet" "dennis" "emma" "jerry" "carolyn" "tyler" "maria" "aaron" "heather" "henry" "diane" "jose" "julie" "douglas" "joyce" "peter" "evelyn" "adam" "joan" "nathan" "victoria" "zachary" "kelly" "walter" "christina" "kyle" "lauren" "harold" "frances" "carl" "martha" "jeremy" "judith" "gerald" "cheryl" "keith" "megan" "roger" "andrea" "arthur" "olivia" "terry" "ann" "lawrence" "jean" "sean" "alice" "christian" "jacqueline" "ethan" "hannah" "austin" "doris" "joe" "kathryn" "albert" "gloria" "jesse" "teresa" "willie" "sara" "billy" "janice" "bryan" "marie" "bruce" "julia" "noah" "grace" "jordan" "judy" "dylan" "theresa" "ralph" "madison" "roy" "beverly" "alan" "denise" "wayne" "marilyn" "eugene" "amber" "juan" "danielle" "gabriel" "rose" "louis" "brittany" "russell" "diana" "randy" "abigail" "vincent" "natalie" "philip" "jane" "logan" "lori" "bobby" "alexis" "harry" "tiffany" "johnny" "kayla" "carlos");
EMAIL_DOMAINS=("yahoo" "hotmail" "apple" "opendoor" "lookout" "google" "gmail" "microsoft" "reynholm-industries" "loyal3" "vevo" "poptent" "philly" "dundermifflin" "world-of-arthur" "friendface");

echo "How many intelligent suggestions should I generate?"
read TOTAL_SUGGESTIONS

echo "Where should I store the files?"
read FILE_LOCATION

echo "I will generate ${TOTAL_SUGGESTIONS} suggestions and save them ${FILE_LOCATION}."

INDEX=0

while [  $INDEX -lt $TOTAL_SUGGESTIONS ]; do
  RANDOM_FIRST_NAME=${FIRST_NAMES[ $(( RANDOM % ${#FIRST_NAMES[@]} )) ] };
  RANDOM_LAST_NAME=${LAST_NAMES[ $(( RANDOM % ${#LAST_NAMES[@]} )) ] };
  RANDOM_EMAIL_DOMAIN=${EMAIL_DOMAINS[ $(( RANDOM % ${#EMAIL_DOMAINS[@]} )) ] };
  RANDOM_EMAIL_FORMAT=$((1 + RANDOM % 3));
  CAPITALIZED_FIRST_NAME=$(tr '[:lower:]' '[:upper:]' <<< ${RANDOM_FIRST_NAME:0:1})${RANDOM_FIRST_NAME:1};
  CAPITALIZED_LAST_NAME=$(tr '[:lower:]' '[:upper:]' <<< ${RANDOM_LAST_NAME:0:1})${RANDOM_LAST_NAME:1}

  case "$RANDOM_EMAIL_FORMAT" in
    1) RANDOM_EMAIL="`echo $RANDOM_FIRST_NAME | head -c 1`${RANDOM_LAST_NAME}@${RANDOM_EMAIL_DOMAIN}.com";
    ;;
    2) RANDOM_EMAIL="${RANDOM_FIRST_NAME}.${RANDOM_LAST_NAME}@${RANDOM_EMAIL_DOMAIN}.com";
    ;;
    3) RANDOM_EMAIL="${RANDOM_FIRST_NAME}@${RANDOM_EMAIL_DOMAIN}.com";
    ;;
    *) RANDOM_EMAIL="${RANDOM_FIRST_NAME}.${RANDOM_LAST_NAME}@${RANDOM_EMAIL_DOMAIN}.com";
    ;;
  esac

  if [ ! -d "$FILE_LOCATION" ]; then
    mkdir -p $FILE_LOCATION
    touch "${FILE_LOCATION}/${INDEX}.vcf"
  fi
  echo -e "BEGIN:VCARD\nVERSION:3.0\nPRODID:-//Apple Inc.//Mac OS X 10.13.6//EN\nN:${CAPITALIZED_FIRST_NAME};${CAPITALIZED_LAST_NAME};;;\nFN: ${CAPITALIZED_FIRST_NAME}  ${CAPITALIZED_LAST_NAME}\nEMAIL;type=INTERNET;type=pref:${RANDOM_EMAIL}\nEND:VCARD" > "${FILE_LOCATION}/${INDEX}.vcf"

  let INDEX=$INDEX+1
done