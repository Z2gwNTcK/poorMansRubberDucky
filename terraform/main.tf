provider "aws" {
  profile = "default"
  region  = var.region
}

resource "aws_key_pair" "ssh-login" {
  key_name = var.key-name
  public_key = var.public-key
}

resource "aws_instance" "poor_mans_rubber_ducky" {
  ami           = var.ubuntu-server-ami
  instance_type = "t2.micro"

  tags = {
    Name = "poor-mans-rubber-duck-c2"
  }
}