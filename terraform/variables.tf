variable "region" {
    default = "us-west-2"
}

variable "ubuntu-server-ami" {
    default = "ami-06d51e91cea0dac8d"
}

# These variables should be read from the tfvars file
variable "key-name" {}
variable "public-key" {}