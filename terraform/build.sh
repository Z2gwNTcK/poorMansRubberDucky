#!/bin/bash

TFVAR_FILE="aws-c2.tfvars"

clear;
echo -e "=================================================="
echo -e "| Poor Man's Rubber Ducky EC2 Instance Terraform |"
echo -e "|------------------------------------------------|"
echo -e "| The following steps will create a public and   |"
echo -e "| private key pair and create an EC2 instance in |"
echo -e "| terraform which can be connected to using      |"
echo -e "| this key pair.                                 |"
echo -e "==================================================\n"

echo -e "First we need to create an SSH key pair."
read -p "What email address would you like to use for this key pair: " EMAIL_ADDRESS

ssh-keygen -t rsa -b 4096 -C "$EMAIL_ADDRESS" -f "/home/$USER/.ssh/id_poor_mans_rubber_ducky"

PUB_KEY_CONTENTS=$(cat ~/.ssh/id_poor_mans_rubber_ducky.pub)

echo -e "\nCreating terraform.tfvars file with public key contents..."
touch terraform.tfvars
echo "key-name = \"poor-mans-rubber-ducky-ssh-pub\"" > $TFVAR_FILE
printf "public-key = \"$PUB_KEY_CONTENTS\"\n" >> $TFVAR_FILE


echo -e "Initializing terraform..."
terraform init

echo -e "Running terraform apply with the tfvars file..."
terraform apply -var-file="$TFVAR_FILE"

